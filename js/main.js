// Giống bài Todo API có thể tái chế code và mần theo api todo có sẵn
// Link với API data => để ra vài task random
const BASE_URL = "https://635f4b16ca0fe3c21a991cb9.mockapi.io";

// Tạo biến idEdit để dùng khi chỉnh sửa id

var idEdited = null;

function fetchAllTodo() {
  //render all todos services
  axios({
    url: `${BASE_URL}/todos`,
    method: "GET",
  })
    //Nếu thành công => dùng .then
    .then(function (res) {
      renderTodoList(res.data);
    })
    // Nếu đéo xong => dùng .catch
    .catch(function (err) {
      console.log("err: ", err);
    });
};

// chạy lần đầu khi load trang
fetchAllTodo();

// Add Todos
function addTodo() {
  var data = layThongTinTuForm();

  var newTodo = {
    desc: data.desc,
    isComplete: false, 
    // Biến mới khi thêm vào sẽ là chưa hoàn thành
  };
  axios({
    url: `${BASE_URL}/todos`,
    method: "POST",
    data: newTodo,
  })
    //Nếu thành công ~ dùng .then
    .then(function (res) {
      // thêm vào để clear giá trị sau khi thêm giá trị vào
      document.getElementById("newTask").value = "";

      // console.log("res todos: ", res.data);
      fetchAllTodo();
      console.log("res: ", res);
    })
    // Nếu thất bại ~ dùng .catch
    .catch(function (err) {
      console.log("err: ", err);
    });
};

//Function xóa todo task

function removeTodo(idTodo) {
  axios({
    url: `${BASE_URL}/todos/${idTodo}`,
    method: "DELETE",
  })
    .then(function (res) {
      // gọi lại API get all todos
      fetchAllTodo();
      console.log("res: ", res);
    })
    .catch(function (err) {
      console.log("err: ", err);
    });
};

function changeStatus() {
  var statusTodo = document.getElementById("statusToDo");
  let count = 0;
  if (statusTodo.checked) {
    alert("CYKA BLYAT");
  }
};
